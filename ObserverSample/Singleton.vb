﻿Public Class Singleton

    Private Sub New()
        Idioma = New Idioma()
    End Sub

    Private Shared _instance As Singleton
    Public Shared Function GetInstance() As Singleton
        If _instance IsNot Nothing Then
            Return _instance
        Else
            _instance = New Singleton()
            Return _instance
        End If
    End Function

    Public Property Idioma As Idioma


End Class
