﻿Public Class Idioma
    Inherits Subject

    Private _idioma As IdiomaEnum
    Public Property Idioma As IdiomaEnum
        Get
            Return _idioma
        End Get
        Set(value As IdiomaEnum)
            _idioma = value
            Notify(Me)
        End Set
    End Property
End Class
