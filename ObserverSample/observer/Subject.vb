﻿Public MustInherit Class Subject

    Private Shared _observers As New List(Of IObserver)

    Public Shared Sub AddObserver(observer As IObserver)
        _observers.Add(observer)
    End Sub


    Public Shared Sub RemoveObserver(observer As IObserver)
        _observers.Remove(observer)
    End Sub

    Public Shared Sub Notify(idioma As Idioma)
        For Each observer In _observers
            observer.Update(idioma)
        Next
    End Sub




End Class
