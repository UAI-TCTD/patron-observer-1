﻿Imports ObserverSample

Public Class Form2
    Implements IObserver

    Public Sub Update(idioma As Idioma) Implements IObserver.Update
        If idioma.Idioma = IdiomaEnum.Español Then
            Me.Text = "form2 en español"
        Else
            Me.Text = "form2 en ingles"
        End If
    End Sub

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Subject.AddObserver(Me)
    End Sub

    Private Sub Form2_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Subject.RemoveObserver(Me)
    End Sub
End Class