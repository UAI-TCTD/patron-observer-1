﻿Imports System.Windows.Forms
Imports ObserverSample

Public Class frmPrincipal
    Implements IObserver

    Public Sub Update(idioma As Idioma) Implements IObserver.Update
        If idioma.Idioma = IdiomaEnum.Español Then
            Me.Text = "Bienvenidos"
        Else
            Me.Text = "Welcome"
        End If
    End Sub

    Private Sub EspañolToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles EspañolToolStripMenuItem.Click
        Singleton.GetInstance.Idioma.Idioma = IdiomaEnum.Español
    End Sub

    Private Sub InglésToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InglésToolStripMenuItem.Click
        Singleton.GetInstance.Idioma.Idioma = IdiomaEnum.Ingles
    End Sub

    Private Sub frmPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Subject.AddObserver(Me)
    End Sub

    Private Sub Form1ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Form1ToolStripMenuItem.Click
        Dim frm As New Form1
        frm.MdiParent = Me
        frm.Show()

    End Sub

    Private Sub Form2ToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Form2ToolStripMenuItem.Click
        Dim frm As New Form2
        frm.MdiParent = Me
        frm.Show()
    End Sub
End Class
