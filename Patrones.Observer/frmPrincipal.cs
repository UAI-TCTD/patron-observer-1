﻿using Patrones.Observer.IdiomaObserver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patrones.Observer
{
    public partial class frmPrincipal : Form , IIdiomaObserver
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void form2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new Form2();
            frm.MdiParent = this;
            frm.Show();
        }

        private void form1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var frm = new Form1();
            frm.MdiParent = this;
            frm.Show();
        }

        public void Update(Idioma idioma)
        {
            if(idioma.IdiomaSelected == IdiomaEnum.Espanol)
            {
                this.Text = "Bienvenidos";
            
            }
            else if(idioma.IdiomaSelected == IdiomaEnum.English)
            {
                this.Text = "Welcome";
            }
        }

        private void espanolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Singleton.GetInstance().Idioma.IdiomaSelected = IdiomaEnum.Espanol;
        }

        private void englishToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Singleton.GetInstance().Idioma.IdiomaSelected = IdiomaEnum.English;
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            Subject.AddObserver(this);
        }
    }
}
