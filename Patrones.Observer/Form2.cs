﻿using Patrones.Observer.IdiomaObserver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Patrones.Observer
{
    public partial class Form2 : Form , IIdiomaObserver
    {
        public Form2()
        {
            InitializeComponent();
        }

        public void Update(Idioma idioma)
        {
            if (idioma.IdiomaSelected == IdiomaEnum.Espanol)
            {
                this.Text = "Form 1 Bienvenidos";

            }
            else if (idioma.IdiomaSelected == IdiomaEnum.English)
            {
                this.Text = "Form 1 Welcome";
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Subject.AddObserver(this);
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Subject.RemoveObserver(this);
        }
    }
}
