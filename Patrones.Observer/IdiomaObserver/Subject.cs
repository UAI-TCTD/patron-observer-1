﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Observer.IdiomaObserver
{
    public abstract class Subject
    {
        
        private static List<IIdiomaObserver> _observers = new List<IIdiomaObserver>();

        public static void AddObserver(IIdiomaObserver observer)
        {
            _observers.Add(observer);
        }


        public static void RemoveObserver(IIdiomaObserver observer)
        {
            _observers.Remove(observer);
        }

        public static void Notify(Idioma idioma)
        {
            foreach (var observer in _observers)
                observer.Update(idioma);
        }
        
    }
}
