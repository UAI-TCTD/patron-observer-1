﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Observer.IdiomaObserver
{
    public interface IIdiomaObserver
    {
        void Update(Idioma idioma);
    }
}
