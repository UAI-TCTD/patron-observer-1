﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Observer
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.CompilerServices;
    using System.Runtime.InteropServices;
    using System.Security;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.VisualBasic;

    public class Singleton
    {
        private Singleton()
        {
            Idioma = new Idioma();
        }

        private static Singleton _instance;
        public static Singleton GetInstance()
        {
            if (_instance != null)
                return _instance;
            else
            {
                _instance = new Singleton();
                return _instance;
            }
        }

        public Idioma Idioma { get; set; }
    }

}
