﻿using Patrones.Observer.IdiomaObserver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones.Observer
{
    public class Idioma : Subject
    {
        private IdiomaEnum _idioma;
        public IdiomaEnum IdiomaSelected
        {
            get
            {
                return _idioma;
            }
            set
            {
                _idioma = value;
                Notify(this);
            }
        }
    }

}
